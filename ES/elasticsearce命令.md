# elasticsearce命令



/usr/app/es/elasticsearch/bin$ ./elasticsearch -d



sudo ./kibana --allow-root 



sudo ./kibana --allow-root 



# 指定分词器

```json
# 
GET _analyze
  {
    "analyzer": "ik_smart",
    "text": "大数据开发工程师"
  }
 #最细粒度拆分 
    GET _analyze
  {
    "analyzer": "ik_max_word",
    "text": "大数据开发工程师"
  } 
```



## Rest操作

> PUT 提交

```json
PUT  /索引名/类型名/文档id
{
    请求体
}

 
   GET _analyze
  {
    "analyzer": "ik_smart",
    "text": "林晞努力"
  }
  
    GET _analyze
  {
    "analyzer": "ik_max_word",
    "text": "林晞努力"
  } 
  
  PUT /test1/type1/1
  {
    "name": "林晞",
    "age": 18
  }
  
#创建表字段类型
  PUT /test2
  {
    "mappings": {
      "properties": {
        "name": {
          "type": "text"
        },
        "age": {
          "type": "long"
        },
        "birthday": {
          "type": "date"
        }
      }
    }
  }
  
  PUT /test2/_doc/1
  {
    "name": "张三",
    "age": 18,
    "birthday": "2021-05-02"
  }
#put修改
   PUT /test2/_doc/1
  {
    "name": "zhangsan",
    "age": 18,
    "birthday": "1999-04-02"
  }


```

> POST修改

```json
  POST /test2/_doc/1/_update
  {
    "doc": {
      "name": "李四"
    }
  }
```

> GET查询

```json
#获取表信息  
  GET test2
  
  GET  /test2/_doc/1

  GET test2/_doc/_search?q=name:张三

  GET _cat/health
  
  GET _cat/shards
```

> DELETE删除

```json
 DELETE /test2/_doc/1
  DELETE  test2
```



## 复杂查询select（排序/分页/高亮/模糊查询/精准查询）  

```json
GET test2/_doc/_search
{
  "query": {
    "match": {
      "name": "三"
    }
  }
}

GET test2/_doc/_search
{
  "query": {
    "match": {
      "name": "三"
    }
  },
#结果过滤
  "_source": ["name","args"]
}

GET test2/_doc/_search
{
  "query": {
    "match": {
      "name": "三"
    }
  },
#排序
  "sort": [
    {
    #排序字段
      "age": {
        "order": "desc"
      }
    }],
#分页
   "from": 0,#从第几个开始，下标从0开始
   "size": 2 #返回多岁条数据
}

GET test2/_doc/_search
{
  "query": {
   #bool查询
    "bool": {
      #must表示and，全部符合
      "must": [
        {
          "match": {
            "name": "张三"
          }
        },
        {
          "match": {
            "age": 16
          }
        }
        ]
    }
  }
}



GET test2/_doc/_search
{
  "query": {
    "bool": {
      #should表所or，或
      "should": [
        {
          "match": {
            "name": "张三"
          }
        },
        {
          "match": {
            "age": 6
          }
        }
        ]
    }
  }
}

GET test2/_doc/_search
{
  "query": {
    "bool": {
     #表示否定
      "must_not": [
        {
          "match": {
            "name": "张三"
          }
        },
        {
          "match": {
            "age": 16
          }
        }
        ]
    }
  }
}

GET test2/_doc/_search
{
  "query": {
    "bool": {
      "must_not": [
        {
          "match": {
            "name": "张三"
          }
        },
        {
          "match": {
            "age": 16
          }
        }
        ],
        #过滤器，大于0，小于50
        "filter": {
          "range": {
            "age": {
              "gt": 0,
              "lt": 50
            }
          }
        }
    }
  }
}

gt >
gte >=
lt <
lte <=

GET test2/_search
{
  "query": {
    "match": {
      #多条件查询用空格隔开
      "name": "三 四"
    }
  }
}
```



> 精确查询！

term 查询是直接通过倒排索引指定的词条精确查找的。

```json
#测试数据
PUT testdb
{
  "mappings": {
    "properties": {
      "name": {
        "type": "text"
      },
      "desc": {
        "type": "keyword"
      }
    }
  }
}

PUT testdb/_doc/1
{
  "name": "林晞努力",
  "desc": "林晞desc"
}

PUT testdb/_doc/2
{
  "name": "林晞努力2",
  "desc": "林晞desc2"
}
```

```json
#分词器
#整体查询，没有被分析
GET _analyze
{
  "analyzer": "keyword",
  "text": "林晞努力"
}

打印==>
{
  "tokens" : [
    {
      "token" : "林晞努力",
      "start_offset" : 0,
      "end_offset" : 4,
      "type" : "word",
      "position" : 0
    }
  ]
}

#被分析
GET _analyze
{
  "analyzer": "standard",
  "text": "林晞努力"
}
打印==>
{
  "tokens" : [
    {
      "token" : "林",
      "start_offset" : 0,
      "end_offset" : 1,
      "type" : "<IDEOGRAPHIC>",
      "position" : 0
    },
    {
      "token" : "晞",
      "start_offset" : 1,
      "end_offset" : 2,
      "type" : "<IDEOGRAPHIC>",
      "position" : 1
    },
    {
      "token" : "努",
      "start_offset" : 2,
      "end_offset" : 3,
      "type" : "<IDEOGRAPHIC>",
      "position" : 2
    },
    {
      "token" : "力",
      "start_offset" : 3,
      "end_offset" : 4,
      "type" : "<IDEOGRAPHIC>",
      "position" : 3
    }
  ]
}

GET testdb/_search
{
  "query": {
    "term": {
      "desc": "林晞desc"
    }
  }
}

打印==>
{
  "took" : 0,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 1,
      "relation" : "eq"
    },
    "max_score" : 0.6931471,
    "hits" : [
      {
        "_index" : "testdb",
        "_type" : "_doc",
        "_id" : "1",
        "_score" : 0.6931471,
        "_source" : {
          "name" : "林晞努力",
          "desc" : "林晞desc"
        }
      }
    ]
  }
}

```

<font color="red">字段类型为text，会被分词器解析</font>

<font color="red">字段类型为keyword，不会被分词器解析</font>

> 高亮显示

```json
GET testdb/_search
{
  "query": {
    "match": {
      "name": "林晞"
    }
  },
  "highlight": {
    "pre_tags": "<p class='key' style='color:red'>",
    "post_tags": "</p>" ,
    "fields": {
      "name": {}
    }
  }
}
```

- 匹配
- 按条件匹配
- 精确匹配
- 区间范围匹配
- 匹配字段过滤
- 多条件查询
- 高亮查询修



## ES API 使用（springboot）

> 新建springboot项目，修改es依赖版本

![image-20210617150201312](./img/image-20210617150201312.png)

> 创建/查询/删除索引

ElasticSearchConfig.java

```java
package com.linxi.demo.config;


import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticSearchConfig {

    //spring <beans id="restClientBuilder" class="RestClientBuilder">
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));

        return  client;

    }
}

```

User.java

```java
package com.linxi.demo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class User {

    private String name;
    private int age;
}
```

EsDemoApiApplicationTests.java

```java
package com.linxi.demo;

import com.alibaba.fastjson.JSON;
import com.linxi.demo.pojo.User;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class EsDemoApiApplicationTests {

    //面向对象
    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    //测试索引的创建 Request
    @Test
    void testCreateIndex() throws IOException {
        //1.创建索引请求
        CreateIndexRequest request = new CreateIndexRequest("linxi_index");

        //2.执行创建请求 IndicesClient，请求后获得响应
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);

        System.out.println(createIndexResponse);
    }

    //获取索引
    @Test
    void testExistIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest("linxi_index");

        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    //删除索引
    @Test
    void  testDelIndex() throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest("linxi_index");

        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
        System.out.println(delete);
    }

    //添加文档
    @Test
    void testAddDocument() throws IOException {
        //创建对象
        User user = new User("林晞", 16);

        //创建请求
        IndexRequest request = new IndexRequest("linxi_index");

        //规则 put /linxi_index/_doc/1
        request.id("1");
        request.timeout(TimeValue.timeValueSeconds(1));
        //request.timeout("1s");

        //将数据放入请求 json
        request.source(JSON.toJSONString(user), XContentType.JSON);
        //客户端发送请求,获取响应结果
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
        System.out.println(indexResponse.toString());
        System.out.println(indexResponse.status());
    }

    //获取文档,判断是否存在 get /linxi_index/_doc/1
    @Test
    void getExit() throws IOException {
        GetRequest request = new GetRequest("linxi_index", "1");
        //不获取返回的_source的上下文
        request.fetchSourceContext(new FetchSourceContext(false));
        request.storedFields("_none_");

        boolean exists = client.exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);

    }

    //获取文档信息
    @Test
    void getDocument() throws IOException {
        GetRequest request = new GetRequest("linxi_index", "1");

        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsString());
        System.out.println(response);
    }

    //更新文档的信息
    @Test
    void updateDocument() throws IOException {
        UpdateRequest request = new UpdateRequest("linxi_index", "1");
        User user = new User("林晞", 26);
        request.timeout("1s");
        request.doc(JSON.toJSONString(user),XContentType.JSON);
        UpdateResponse update = client.update(request, RequestOptions.DEFAULT);
        System.out.println(update);
    }

    //删除文档记录
    @Test
    void deleteDocument() throws IOException {
        DeleteRequest request = new DeleteRequest("linxi_index", "1");
        request.timeout("1s");
        DeleteResponse delete = client.delete(request, RequestOptions.DEFAULT);
        System.out.println(delete.status());
    }

    //批量插入
    @Test
    void BulkRequest() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("10s");

        ArrayList<User> userList = new ArrayList<>();
        userList.add(new User("linxi1",1));
        userList.add(new User("linxi1",2));
        userList.add(new User("linxi1",3));
        userList.add(new User("linxi1",4));
        userList.add(new User("linxi5",5));
        userList.add(new User("linxi6",6));

        //批处理请求
        for (int i = 0; i < userList.size(); i++) {
            bulkRequest.add(new IndexRequest("linxi_index")
                    .id("" + (i+1))
                    .source(JSON.toJSONString(userList.get(i)),XContentType.JSON));
        }
        BulkResponse bulk = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        //是否失败，false表示没有失败
        System.out.println(bulk.hasFailures());
    }

    //查询
    @Test
    void testSearch() throws IOException {
        SearchRequest request = new SearchRequest("linxi_index");

        //构建搜索条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //查询条件，我们可以使用Queryulder工具来使用
        //QueryBuilders.termQuery精确匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "linxi1");
        //查询匹配所有
        //MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        sourceBuilder.query(termQueryBuilder);
        sourceBuilder.from(0);
        sourceBuilder.size(5);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        request.source(sourceBuilder);
        SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
        System.out.println(JSON.toJSONString(searchResponse.getHits()));
        System.out.println("========================================");
        for (SearchHit documentFields : searchResponse.getHits().getHits()) {
            System.out.println(documentFields.getSourceAsMap());
        }

    }
}

```

