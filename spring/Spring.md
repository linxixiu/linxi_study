# Spring

## 1、IOC控制反转

> IOC原型

```java
    private UserDao userDao;
    @Override
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
```

从本质上解决了多次创建对象。降低了系统耦合性

> 无参构造，直接可以使用以下方式加入到spring容器

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
package com.linxi.pojo;

public class User {
    private String name;
    //无参构造默认可以省略
    public User() {
        System.out.println("无参构造");
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void show(){
        System.out.println("name:"+name);
    }
}

```

<table><tr><td bgcolor=#F5F5F5>beans.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="user" class="com.linxi.pojo.User">
        <property name="name" value="林晞"/>
    </bean>
</beans>
```

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
import com.linxi.pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        User user = (User) context.getBean("user");
        user.show();
    }
}

========>
无参构造
name:林晞
```

> 有参构造

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
package com.linxi.pojo;

public class User {
    private String name;
    //有参构造
    public User(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void show(){
        System.out.println("name:"+name);
    }
}


```

<table><tr><td bgcolor=#F5F5F5>beans.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--第一种构造：下标注入-->
    <!--<bean id="user" class="com.linxi.pojo.User">
        <constructor-arg index="0" value="林晞—有参构造1"/>
    </bean>-->
    <!--第二种：通过类型-->
	<!--<bean id="user" class="com.linxi.pojo.User">
        <constructor-arg type="java.lang.String" value="林晞-有参构造2"/>
    </bean>-->
    <!--第三种：通过参数名-->
    <bean id="user" class="com.linxi.pojo.User">
        <constructor-arg name="name" value="林晞-有参构造3"/>
    </bean>
</beans>
```

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
import com.linxi.pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        User user = (User) context.getBean("user");
        user.show();
    }
}

========>
name:林晞—有参构造1
========>
name:林晞-有参构造2
========>
name:林晞-有参构造3
```

总结：在配置文件加载的时候，容器中管理的对象就被初始化了。

## 2、Spring配置

> 别名

```xml
 <alias name="user" alias="uu"/>
```

> Bean的配置

```xml
<!--
id		: bean的唯一标识，也是相当于我们学的对象名
class	：bean对象所对应的全限定名：包名+类名
name	：也是别名
-->
    <bean id="user" class="com.linxi.pojo.User" name="u1,u2 u3;u4">
        <constructor-arg name="name" value="林晞-有参构造"/>
    </bean>
```

> import

一般用于团队开发使用，可以将多个配置文件，导入合并为一个

假设，现在项目中有多个人开发，将三个人复制的不同的类开发，不同的类注册在不同的bean中，我们可以通过import将所有的bean.xml导入

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--导入多个bean-->
    <import resource="beans1.xml"/>
    <import resource="beans2.xml"/>
    <import resource="beans3.xml"/>
</beans>
```

## 3.DI依赖注入

> 构造器注入

> **Set注入**

- 依赖注入：Set注入

  - 依赖：bean对象中创建依赖于容器

  - 注入：bean对象中的所有属性，由容器来注入

    

<font color="green" size=5>环境搭建</font>

1.复杂类型

2.真实测试

<table><tr><td bgcolor=#F5F5F5>Address.java</td></tr></table>

```java
package com.linxi.pojo;

public class Address {
    private String address;
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                '}';
    }
}
```

<table><tr><td bgcolor=#F5F5F5>Student.java</td></tr></table>

```java
package com.linxi.pojo;

import java.util.*;

public class Student {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbys;
    private Map<String,String> card;
    private Set<String> games;
    private String wife;
    private Properties info;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public String[] getBooks() {
        return books;
    }
    public void setBooks(String[] books) {
        this.books = books;
    }
    public List<String> getHobbys() {
        return hobbys;
    }
    public void setHobbys(List<String> hobbys) {
        this.hobbys = hobbys;
    }
    public Map<String, String> getCard() {
        return card;
    }

    public void setCard(Map<String, String> card) {
        this.card = card;
    }
    public Set<String> getGames() {
        return games;
    }
    public void setGames(Set<String> games) {
        this.games = games;
    }
    public String getWife() {
        return wife;
    }
    public void setWife(String wife) {
        this.wife = wife;
    }
    public Properties getInfo() {
        return info;
    }
    public void setInfo(Properties info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", address=" + address.toString() +
                ", books=" + Arrays.toString(books) +
                ", hobbys=" + hobbys +
                ", card=" + card +
                ", games=" + games +
                ", wife='" + wife + '\'' +
                ", info=" + info +
                '}';
    }
}

```

<table><tr><td bgcolor=#F5F5F5>beans.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="address" class="com.linxi.pojo.Address">
        <property name="address" value="北京"/>
    </bean>
    <bean id="student" class="com.linxi.pojo.Student">
        <!--普通注入-->
        <property name="name" value="林晞"/>
        <!--类注入-->
        <property name="address" ref="address"/>
        <!--数组注入-->
        <property name="books">
            <array>
                <value>JAVA编程思想</value>
                <value>HIVE编程思想</value>
                <value>三国演义</value>
            </array>
        </property>
        <!--list-->
        <property name="hobbys">
            <list>
                <value>唱歌</value>
                <value>打游戏</value>
                <value>看电影</value>
            </list>
        </property>
        <!--Map-->
        <property name="card">
            <map>
                <entry key="身份证" value="12345789"/>
                <entry key="银行卡" value="123123213"/>
            </map>
        </property>
        <!--set-->
        <property name="games">
            <set>
                <value>LOL</value>
                <value>CF</value>
                <value>CS</value>
                <value>DNF</value>
            </set>
        </property>
        <!--NULL-->
        <property name="wife">
            <null/>
        </property>
        <!--Propertise-->
        <property name="info">
            <props>
                <prop key="学号">19940906</prop>
                <prop key="学历">本科</prop>
            </props>
        </property>
    </bean>

</beans>
```



> 拓展方式

<table><tr><td bgcolor=#F5F5F5>userbeans.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:c="http://www.springframework.org/schema/c"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--p命名空间注入，可以直接属性注入-->
    <bean id="user" class="com.linxi.pojo.User" p:name="林晞" p:age="16"/>

    <!--c命名空间注入，可以直接属性注入,需要在User中加有参构造-->
    <bean id="user2" class="com.linxi.pojo.User" c:name="林晞2" c:age="19 "/>
</beans>
```

p命名空间和c命名空间不能直接使用，需要导入xml约束

```xml
xmlns:p="http://www.springframework.org/schema/p"
xmlns:c="http://www.springframework.org/schema/c"
```



> bean的作用域

1.单例模式，默认

```xml
<bean id="user" class="com.linxi.pojo.User"  scope="singleton"/>
```

2.原型模式：每次get的时候。都会产生新对象

```xml
<bean id="user" class="com.linxi.pojo.User" p:name="林晞" p:age="16" scope="prototype"/>
```

3.其余的request、session、application这些个只能在web开发中使用

## 4、Bean的自动装配

- 自动装配是spring满足bean依赖的一种方式！
- Spring会在上下文中自动寻找，并自动给bean装配属性！

在Spring中有三种装配的方式

1. 在xml中显示配置
2. 在java中显示配置
3. **隐式的自动装配bean**

> 使用ByType、ByName自动装配

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="cat" class="com.linxi.pojo.Cat"/>
    <bean id="dog" class="com.linxi.pojo.Dog"/>

    <bean id="perple" class="com.linxi.pojo.Perple">
        <property name="name" value="林晞"/>
        <property name="cat" ref="cat"/>
        <property name="dog" ref="dog"/>
    </bean>
</beans>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="cat" class="com.linxi.pojo.Cat"/>
    <bean id="dog" class="com.linxi.pojo.Dog"/>
	<!--
	ByName: 会自动在容器上下文中查找，和自己对象set方法后面的值对应的beanid！
	-->
    <bean id="perple" class="com.linxi.pojo.Perple" autowire="byName">
    <!--
	ByName: 会自动在容器上下文中查找，和自己属性相同的bean!
	-->   
    <bean id="perple" class="com.linxi.pojo.Perple" autowire="byType">
        <property name="name" value="林晞"/>
    </bean>
</beans>
```

- ByName的时候，需要保证Bean的id唯一
- ByType的时候，需要保证bean的class唯一

> 使用注解实现自动装配

要使用注解须知：

1. 导入约束
2. 配置注解支持

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
	<!--配置注解支持-->
    <context:annotation-config/>
    
    </bean>
</beans>
```

> **@Autowired**

直接在属性上使用即可，也可以使用set方式！

```xml
@Nullable 字段标记了解这个注解，说明这个字段可以为null
```

如果@Autowired自动装配的环境比较复杂，自动装配无法通过一个注解@Autowired完成的时候，我们可以使用@Qualifier(value="dog")去配置@Autowired的使用，指定一个唯一的bean对象注入。

> @Qualifier

```java
    @Autowired
    @Qualifier(value = "dog")
    private Dog dog;
```

当bean中有相同类型不同名称的，无法自动装配，需要指定name

> @Resource

```java
    @Resource(name = "cat")
    private Cat cat;
```

小结：

@Resource和@Autowired的区别：

- 都是用来自动装配，都可以放在属性字段上
- Autowired通过byname的方式实现，而且必须要求这个对象存在。[常用]
- @Resource通过默认的byname方式实现，如果名字找不到名字，则通过buType实现！如果两个都找不到的情况下就报错。
- 执行顺序不同：@Autowired通过buname的方式实现

## 5、使用注解开发

- 在spring4之后，必须导入aop包。
- 在使用注解需要导入、context约束，

>bean

> 属性如何注入

```java
//等价于 <bean id="user" class="com.linxi.pojo.User"/>
@Component
public class User {
    //相当于<property name="name" value="linxi"/>
    //@Value("linxi")
    public String name;
    @Value("linxi")
    public void setName(String name) {
        this.name = name;
    }
}
```

>  衍生纾解

@Component有几个衍生注解，我们在web开发中，会按照MVC三层架构分层。

- dao			:	@Repository

- service     : @Service

- controller   :   @controller

  以上四个注解功能都是一样的，都是将某个类装配到spring中。

> 自动装配置

- @Autowired
- @Resource
- @Nullable

> 作用域

```java
@Component
@Scope("prototype")
public class User {
    @Value("linxi")
    public String name;
}
```

xml与注解：

- xml更加万能，适用于任何场景。
- 注解不是自己类使用不了，维护相对复杂

最佳实践：

- xml用来管理bean
- 注解只负责完成属性注入
- 我们在使用的过程中，只需要注意一个问题：必须让注解生效，就需要开启注解的支持

```xml
    <!--指定要扫描的包-->
    <context:component-scan base-package="com.linxi"/>
    <context:annotation-config/>
```

## 6、使用java的方式配置Spring

我们现在要完全不使用SPring的xml配置了，全权交给java来做。

javaConfig是spring的一个子项目。

<table><tr><td bgcolor=#F5F5F5>LinxiConfig.java</td></tr></table>

```java
package com.linxi.config;

import com.linxi.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.linxi.pojo")
@Import(LinxiConfig2.class)
public class LinxiConfig {
    /**
     * 注册一个bean，就相当于我们之前写的bean标签
     * 这个方法名字，就相当于bean标签的id
     * 这个返回值就相当于bean中的class
     * @return：就是返回要注入到bean的对象
     */
    @Bean
    public User getUser(){
        return new User();
    }
}
```

<table><tr><td bgcolor=#F5F5F5>LinxiConfig2.java</td></tr></table>

```java
package com.linxi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.linxi.pojo")
public class LinxiConfig2 {
}
```

<table><tr><td bgcolor=#F5F5F5>User.java</td></tr></table>

```java
package com.linxi.pojo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
@Component
public class User {
    private String name;
    public String getName() {
        return name;
    }
    @Value("林晞")
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
```

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
import com.linxi.config.LinxiConfig;
import com.linxi.pojo.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        //如果完全使用配置类方式去做，我们就只能通过AnnotationConfig上下文来获取容器
        ApplicationContext context = new AnnotationConfigApplicationContext(LinxiConfig.class);
        User user = (User) context.getBean("getUser");
        System.out.println(user.getName());
    }
}

```

## 7、代理模式

代理模式就是SpringAOP的地层！

> 代理模式分类

1. 静态代理
2. 动态代理

### 7.1 静态代理

角色分析：

- 抽象角色：一般会使用接口或者抽象类来解决
- 真实角色：被代理的角色
- 代理角色：代理真实角色，代理真实角色后，我们一般会做一些附属操作
- 客户：访问代理对象的人

<table><tr><td bgcolor=#F5F5F5>Rent.java</td></tr></table>

```java
package com.linxi.demo01;

//租房
public interface Rent {
    public void rant();
}
```

<table><tr><td bgcolor=#F5F5F5>Host.java</td></tr></table>

```java
package com.linxi.demo01;

//房主
public class Host implements Rent{
    @Override
    public void rant() {
        System.out.println("房东出租房子");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>Proxy.java</td></tr></table>

```java
package com.linxi.demo01;
public class Proxy implements Rent{
    private Host host;
    public Proxy() {
    }
    public Proxy(Host host) {
        this.host = host;
    }
    @Override
    public void rant() {
        host.rant();
        seeHouse();
        hetong();
        fare();
    }
    //看房
    public void seeHouse(){
        System.out.println("带看房子");
    }
    //服务费
    public void fare(){
        System.out.println("收服务费");
    }
    //签合同
    public void hetong(){
        System.out.println("签合同");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>Client.java</td></tr></table>

```java
package com.linxi.demo01;

public class Client {
    public static void main(String[] args) {
        Host host = new Host();
        //代理中介邦房东租房子，会有附属操作
        Proxy proxy = new Proxy(host);
        proxy.rant();
    }
}
```

> 代理模式的好处

- 可以使真实角色更加纯粹，不用去关注一些公共业务
- 公共也就交给代理角色，实现了业务的分工
- 公共业务发生扩展的时候，方便集中管理

> 缺点

一个真实角色就要产生一个代理角色，代码量会翻倍

### 7.1 动态代理

- 动态代理和静态代理角色一样

- 动态代理类是动态生成的，不是我们直接写好的

- 动态代理分为两大类：

  - 基于接口的动态代理
  - 基于类的动态代理

  基于接口——JDK动态代理

  基于：cglib

  基于java字节码实现：javasist

需要了解两个类：proxy，Invocationandler

<table><tr><td bgcolor=#F5F5F5>UserService.java</td></tr></table>

```java
package com.linxi.demo02;
public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
```

<table><tr><td bgcolor=#F5F5F5>UserServiceImpl.java</td></tr></table>

```java
package com.linxi.demo02;
public class UserServiceImpl implements UserService{
    @Override
    public void add() {
        System.out.println("增加了一个用户");
    }
    @Override
    public void delete() {
        System.out.println("删除了一个用户");
    }
    @Override
    public void update() {
        System.out.println("修改了一个用户");
    }
    @Override
    public void query() {
        System.out.println("查询了一个用户");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>ProxyInvocationHandler.java</td></tr></table>

```java
package com.linxi.demo04;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

//我们会用这个，自动生成代理类
public class ProxyInvocationHandler implements InvocationHandler {
    //被代理的接口
    private Object target;
    public void setTarget(Object target) {
        this.target = target;
    }
    //生成得到代理类
    public Object getProxy(){
        Object proxyInstance = Proxy.newProxyInstance(
                this.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                this);
        return proxyInstance;
    }
    //处理代理对象，并返回结果
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //动态代理的本质，就是使用反射机制实现
        log(method.getName());
        Object result = method.invoke(target, args);
        return result;
    }
    public void log(String msg){
        System.out.println("执行了"+msg+"方法");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>Client.java</td></tr></table>

```java
package com.linxi.demo04;
import com.linxi.demo02.UserService;
import com.linxi.demo02.UserServiceImpl;
public class Client {
    public static void main(String[] args) {
        //真实角色
        UserServiceImpl userService = new UserServiceImpl();
        //代理角色，不存在
        ProxyInvocationHandler pih = new ProxyInvocationHandler();
        //设置要代理的对象
        pih.setTarget(userService);
        //动态生成代理类
        UserService proxy = (UserService) pih.getProxy();
        proxy.add();
        proxy.delete();
    }
}
```

> 动态代理的好处

- 可以使真实角色更加纯粹，不用去关注一些公共业务
- 公共也就交给代理角色，实现了业务的分工
- 公共业务发生扩展的时候，方便集中管理
- 一个动态代理类代理的是一个接口，一般就是对应的一类业务
- 一个动态代理类可以代理多个类，只要实现同一个接口即可

## 8、AOP：面向切面编程

> 导入依赖

```xml
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.4</version>
</dependency>
```

> 方式一：使用原生的spring API接口实现

<table><tr><td bgcolor=#F5F5F5>AfterLog.java</td></tr></table>

```java
package com.linxi.log;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

public class AfterLog implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("执行了" + method.getName() + "返回的结果为" + returnValue);
    }
}
```

<table><tr><td bgcolor=#F5F5F5>Log.java</td></tr></table>

```java
package com.linxi.log;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class Log implements MethodBeforeAdvice {
    /**
     *
     * @param method    要执行的目标对象的方法
     * @param args      参数
     * @param target    目标对象
     * @throws Throwable
     */
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println(target.getClass().getName() + "的" + method.getName() + "被执行了");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>UserService.java</td></tr></table>

```java
package com.linxi.service;

public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
```

<table><tr><td bgcolor=#F5F5F5>UserServiceImpl.java</td></tr></table>

```java
package com.linxi.service;

public class UserServiceImpl implements UserService{
    @Override
    public void add() {
        System.out.println("增加了一个用户");
    }
    @Override
    public void delete() {
        System.out.println("删除了一个用户");
    }
    @Override
    public void update() {
        System.out.println("修改了一个用户");
    }
    @Override
    public void query() {
        System.out.println("查询了一个用户");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>applicationContext.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       https://www.springframework.org/schema/aop/spring-aop.xsd">

    <bean id="userService" class="com.linxi.service.UserServiceImpl"/>
    <bean id="log" class="com.linxi.log.Log"/>
    <bean id="afterlog" class="com.linxi.log.AfterLog"/>

    <!--方式一：使用原生的spring API-->
    <!--配置aop:导入aop约束-->
    <aop:config>
        <!--切入点expression:表达式，execution(要执行的位置！)-->
        <aop:pointcut id="pointcut"  expression="execution(* com.linxi.service.UserServiceImpl.*(..))"/>
        <!--执行环绕增强-->
        <aop:advisor advice-ref="log" pointcut-ref="pointcut"/>
        <aop:advisor advice-ref="afterlog" pointcut-ref="pointcut"/>
    </aop:config>
</beans>
```

<table><tr><td bgcolor=#F5F5F5>MyTest.java</td></tr></table>

```java
import com.linxi.service.UserService;
import com.linxi.service.UserServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //代理的是接口
        UserService userService = context.getBean("userService", UserService.class);
        userService.add();
    }
}
```

> 方式二：自定义切面，比较简单

<table><tr><td bgcolor=#F5F5F5>applicationContext.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       https://www.springframework.org/schema/aop/spring-aop.xsd">

    <bean id="userService" class="com.linxi.service.UserServiceImpl"/>
    <bean id="log" class="com.linxi.log.Log"/>
    <bean id="afterlog" class="com.linxi.log.AfterLog"/>
    <!--方式二：自定义类-->
    <bean id="div" class="com.linxi.div.DivPointCut"/>
    <aop:config>
        <!--自定义切面，ref引用的类-->
        <aop:aspect ref="div">
            <!--切入点-->
            <aop:pointcut id="point" expression="execution(* com.linxi.service.UserServiceImpl.*(..))"/>
            <!--通知-->
            <aop:before method="before" pointcut-ref="point"/>
            <aop:after method="after" pointcut-ref="point"/>
        </aop:aspect>
    </aop:config>

</beans>
```

> 方式三：使用注解方式实现

<table><tr><td bgcolor=#F5F5F5>AnnotationPointCut.java</td></tr></table>

```java
package com.linxi.div;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

//方式三：使用注解方式实现
@Aspect
public class AnnotationPointCut {
    @Before("execution(* com.linxi.service.UserServiceImpl.*(..))")
    public void before(){
        System.out.println("--------方法执行前--------");
    }
        @After("execution(* com.linxi.service.UserServiceImpl.*(..))")
    public void after(){
        System.out.println("--------方法执行后--------");
    }
        @Around("execution(* com.linxi.service.UserServiceImpl.*(..))")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("*********方法执行前*********");
        System.out.println(proceedingJoinPoint.getSignature());
        proceedingJoinPoint.proceed();
        System.out.println("*********方法执行前*********");
    }
}
```

<table><tr><td bgcolor=#F5F5F5>applicationContext.xml</td></tr></table>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       https://www.springframework.org/schema/aop/spring-aop.xsd">

    <bean id="userService" class="com.linxi.service.UserServiceImpl"/>
    <bean id="log" class="com.linxi.log.Log"/>
    <bean id="afterlog" class="com.linxi.log.AfterLog"/>

    <!--方式三：注解方式-->
    <bean id="annotationPointCut" class="com.linxi.div.AnnotationPointCut"/>
    <!--开启注解支持-->
    <aop:aspectj-autoproxy/>

</beans>
```

9、整合mybatis

> 

修改密码：
update user set authentication_string = PASSWORD('linxi123') where user='root';
update user set plugin="mysql_native_password";
FLUSH PRIVILEGES;

 set password for 'root'@localhost = password(‘linxi123’); 

 set password for root@localhost = password(‘root’); 

ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';
