# Git

## 1.git常用基本命令

> 1.git配置

使用git前，需要在本地配置git远程仓库；

```shell
git config --global user.name "user_name"
git config --global user.email "user@*.com"
#检查信息是否写入成功
git config --list 
```



> 2.代码提交

本地修改的代码提交到仓库中，起到版本管理以及防止代码丢失；

```shell
git add .	#将当前目录的提交到git暂存区
git commit -m '注释本次提交的内容' #将文件提交到本地仓库
git
```



> 3.文件回退

文件回退保证提交了错误的代码，找回旧版本

```shell
eg. 
echo "hello world" > file_name #已经上传到中央仓库
# 第一种：误操作导致文件丢失
 # 清空文件
 > file_name
 # 回退
 git checkout file_name
 # 验证
 cat file_name
 
# 第二种：提交到了暂存区
 # 清空文件
 > file_name
 # git提交到暂存区
 git add file_name
 git status
 # 回退
 git reset HEAD file_name	#将本地仓库的还原到暂存区
 git checkout -- file_name	#将暂存区的还原到本地
 # 验证
 git status
 
# 第三种：提交多次到本地仓库，需要回退到某个版本
 # git提交到本地仓库
 echo "hello git" >> file_name
 git add file_name
 git commit -m "添加hello git"
 echo "easy git" >> file_name
 git add file_name
 git commit -m "添加easy git"
 # 回退
 git reflog	#查看所有的commitid
 git log --oneline	#查看此刻版本之前的commitid
 git reset --hard commitid
 # 验证
 git status
```

​	Windows测试：

![git回退练习示例](./img/git回退练习示例.png)

> 4.修改文件名称

有时候我们修改git文件名称，需要删除源文件重新上传，不太好验证文件内容是否有变化；

```shell
git mv file_name_old file_name_new
```

### 2.分支管理

> 1.查看分支

```shell
git branch
```

> 2.创建分支

```shell
git branch new_branch_name
git branch
```

> 3.切换分支

```shell
git checkout new_branch_name
```

> 4.测试在分支中创建文件，并合并到主分支（需要将主分支merge到分支）

```shell
git checkout main	#主分支创建文件
touch file{6..9}	#创建文件

git checkout new_branch_name	#new_branch_name分支创建文件
touch file{1..5}	#创建文件
git merge main
git checkout main
git merge new_branch_name
```

> 5.删除分支

```shell
git branch new_branch_name -d
```

> 6.分支合并冲突