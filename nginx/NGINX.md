# NGINX

## 1 Basic Functionality

### 1.1 Controlling NGINX

当修改nginx配置之后，需要执行以下命令生效

```shell
nginx -s <SIGNAL>
```

> where `<SIGNAL>` can be one of the following:
>
> - `quit` – Shut down gracefully
> - `reload` – Reload the configuration file
> - `reopen` – Reopen log files
> - `stop` – Shut down immediately (fast shutdown)

## 2 Overview

