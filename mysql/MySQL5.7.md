# 1 MySQL5.7—在线DDL总结

<font color=red >---切记：DDL操作要在业务低峰期进行</font>

## 1.1 MySQL各版本，对于DDL的处理方式是不同的，主要有三种：

①：<font color=blue>Copy Table</font>方式： 这是InnoDB最早支持的方式。顾名思义，通过临时表拷贝的方式实现的。新建一个带有新结构的临时表，将原表数据全部拷贝到临时表，然后Rename，完成创建操作。这个方式过程中，<u>原表是可读的，不可写</u>。但是会<u>消耗一倍的存储空间</u>。

②：<font color=blue>Inplace</font>方式：这是原生MySQL 5.5，以及innodb_plugin中提供的方式。所谓Inplace，也就是在原表上直接进行，不会拷贝临时表。相对于Copy Table方式，这比较高效率。<u>原表同样可读的，但是不可写</u>。

③：<font color=blue>Online</font>方式：这是MySQL 5.6以上版本中提供的方式，也是今天我们重点说明的方式。无论是Copy Table方式，还是Inplace方式，原表只能允许读取，不可写。对应用有较大的限制，因此MySQL最新版本中，InnoDB支持了所谓的Online方式DDL。与以上两种方式相比，online方式支持DDL时不仅<u>可以读，还可以写</u>，对于dba来说，这是一个非常棒的改进。

## 1.2 MySQL5.7中online ddl：（algorithm=inplace）

<font color=red >ALGORITHM=INPLACE，可以避免重建表带来的IO和CPU消耗，保证ddl期间依然有良好的性能和并发。</font>

ALGORITHM=COPY，需要拷贝原始表，所以不允许并发DML写操作，可读。这种copy方式的效率还是不如 inplace ，因为前者需要记录undo和redo log，而且因为临时占用buffer pool引起短时间内性能受影响。

①：In-Place为Yes是优选项，说明该操作支持INPLACE

②：Copies Table为No是优选项，因为为Yes需要重建表。大部分情况与In-Place是相反的

③：Allows Concurrent DML?为Yes是优选项，说明ddl期间表依然可读写，可以指定 LOCK=NONE（如果操作允许的话mysql自动就是NONE）

④：Allows Concurrent Query?默认所有DDL操作期间都允许查询请求，放在这只是便于参考